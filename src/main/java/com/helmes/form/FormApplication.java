package com.helmes.form;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class FormApplication {
    static Logger logger = Logger.getLogger(FormApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(FormApplication.class, args);
        logger.info("h2 database started on: http://localhost:8080/h2-console");

    }

}
