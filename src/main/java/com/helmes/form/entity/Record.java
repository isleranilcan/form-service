package com.helmes.form.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "record")
@AllArgsConstructor
@NoArgsConstructor
public class Record extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "sectors")
    @ElementCollection
    private List<Long> sectors;

    @Column(name = "is_terms_agreed")
    private boolean isTermsAgreed;
}
