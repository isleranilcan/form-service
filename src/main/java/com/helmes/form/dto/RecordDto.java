package com.helmes.form.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
public class RecordDto {

    private long sessionId;
    @NotBlank(message = "Name")
    private String name;
    @NotEmpty(message = "Sectors")
    private List<Long> sectors;
    @AssertTrue(message = "Agree to terms")
    private boolean isTermsAgreed;
}
