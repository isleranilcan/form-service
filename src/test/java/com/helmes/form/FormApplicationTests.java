package com.helmes.form;

import com.helmes.form.controller.FormController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FormApplicationTests {

    @Autowired
    private FormController formController;

    @Test
    void contextLoads() {
        assertNotNull(formController);
    }

}
