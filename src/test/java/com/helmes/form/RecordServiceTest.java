package com.helmes.form;

import com.helmes.form.dto.RecordDto;
import com.helmes.form.entity.Record;
import com.helmes.form.repository.RecordRepository;
import com.helmes.form.service.RecordService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RecordServiceTest {

    @Mock
    private RecordRepository recordRepository;

    @InjectMocks
    private RecordService recordService;

    @Test
    @DisplayName("Should create record")
    public void testCreateRecord() {
        //given
        Record record = new Record();
        record.setName("name");
        record.setSectors(List.of(1L, 2L, 3L));
        record.setTermsAgreed(true);
        RecordDto expected = RecordDto.builder()
                .name("name")
                .sectors(List.of(1L, 2L, 3L))
                .isTermsAgreed(true)
                .build();
        when(recordRepository.save(any())).thenReturn(record);

        //when
        RecordDto actual = recordService.createRecord(expected);

        //then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should get record")
    public void testGetRecord() {
        //given
        Record record = new Record();
        record.setName("name");
        record.setSectors(List.of(1L, 2L, 3L));
        record.setTermsAgreed(true);
        RecordDto expected = RecordDto.builder()
                .name("name")
                .sectors(List.of(1L, 2L, 3L))
                .isTermsAgreed(true)
                .build();
        when(recordRepository.findById(1L)).thenReturn(Optional.of(record));

        //when
        RecordDto actual = recordService.getRecord(1L);

        //then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should return null if record is not found")
    public void testGetRecordReturnsNull() {
        //given
        when(recordRepository.findById(1L)).thenReturn(Optional.empty());

        //when
        RecordDto actual = recordService.getRecord(1L);

        //then
        assertNull(actual);
    }

    @Test
    @DisplayName("Should return null if record is not found")
    public void testUpdateRecordReturnsNull() {
        //given
        RecordDto recordDto = RecordDto.builder()
                .sessionId(1)
                .name("name")
                .sectors(List.of(1L, 2L, 3L))
                .isTermsAgreed(true)
                .build();
        when(recordRepository.findById(1L)).thenReturn(Optional.empty());

        //when
        RecordDto actual = recordService.updateRecord(recordDto);

        //then
        assertNull(actual);
    }
}
