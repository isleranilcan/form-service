package com.helmes.form.mapper;

import com.helmes.form.dto.CategoryDto;
import com.helmes.form.entity.Category;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CategoryDtoMapper {

    static Logger logger = Logger.getLogger(CategoryDtoMapper.class.getName());

    public static List<CategoryDto> map(Iterable<Category> all) {
        List<CategoryDto> parents = StreamSupport.stream(all.spliterator(), false)
                .filter(category -> category.getParentId() == 0)
                .map(category -> CategoryDto.builder()
                        .id(category.getId())
                        .label(category.getCategoryName())
                        .build())
                .collect(Collectors.toList());
        List<Category> children = StreamSupport.stream(all.spliterator(), false)
                .filter(category -> category.getParentId() != 0)
                .collect(Collectors.toList());

        while (!children.isEmpty()) {
            Iterator<Category> iterator = children.iterator();
            while (iterator.hasNext()) {
                Category item = iterator.next();
                addToChildren(parents, item, iterator);
            }
        }

        logger.info("mapper called");
        return parents;
    }

    private static void addToChildren(List<CategoryDto> childrenList, Category child, Iterator<Category> iterator) {
        if (null != childrenList)
            for (CategoryDto item : childrenList) {
                if (item.getId() == child.getParentId()) {
                    if (item.getChildren() == null)
                        item.setChildren(new ArrayList<>());
                    item.getChildren().add(CategoryDto.builder()
                            .id(child.getId())
                            .label(child.getCategoryName())
                            .build());
                    iterator.remove();
                } else {
                    addToChildren(item.getChildren(), child, iterator);
                }
            }
    }


}
