package com.helmes.form.controller;

import com.helmes.form.dto.CategoryDto;
import com.helmes.form.dto.RecordDto;
import com.helmes.form.service.CategoryService;
import com.helmes.form.service.RecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class FormController {

    private final CategoryService categoryService;
    private final RecordService recordService;

    @GetMapping("/sectors")
    public ResponseEntity<List<CategoryDto>> getSectors() {
        return ResponseEntity.ok(categoryService.getSectors());
    }

    @PostMapping("/record")
    public ResponseEntity<RecordDto> saveRecord(@Valid @RequestBody RecordDto recordDto) {
        return ResponseEntity.ok(recordService.createRecord(recordDto));
    }

    @PutMapping("/record")
    public ResponseEntity<RecordDto> updateRecord(@Valid @RequestBody RecordDto recordDto) {
        RecordDto returnDto = recordService.updateRecord(recordDto);
        if (returnDto == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(returnDto);
    }

    @GetMapping("/record")
    public ResponseEntity<RecordDto> getRecord(@Valid @RequestParam long sessionId) {
        RecordDto returnDto = recordService.getRecord(sessionId);
        if (returnDto == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(returnDto);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
