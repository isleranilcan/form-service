package com.helmes.form.entity;

import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Getter
@EntityListeners({AuditingEntityListener.class})
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    @Id
    @Column(
            name = "id",
            updatable = false
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private long id;

    @Column(name = "created_date",
            updatable = false
    )
    @CreatedDate
    private Instant createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private Instant modifiedDate;
}
