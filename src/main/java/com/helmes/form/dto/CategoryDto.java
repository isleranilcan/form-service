package com.helmes.form.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CategoryDto {
    private long id;
    private String label;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<CategoryDto> children;
}
