package com.helmes.form.service;

import com.google.gson.Gson;
import com.helmes.form.dto.RecordDto;
import com.helmes.form.entity.Record;
import com.helmes.form.mapper.RecordDtoMapper;
import com.helmes.form.repository.RecordRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RecordService {

    private final RecordRepository recordRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public RecordDto createRecord(RecordDto recordDto) {
        Record record = new Record();
        record.setName(recordDto.getName());
        record.setSectors(recordDto.getSectors());
        record.setTermsAgreed(recordDto.isTermsAgreed());

        logger.info("Record is sent to repository: " + new Gson().toJson(record));

        return RecordDtoMapper.map(recordRepository.save(record));
    }

    public RecordDto updateRecord(RecordDto recordDto) {
        Optional<Record> recordById = recordRepository.findById(recordDto.getSessionId());
        if (recordById.isPresent()) {
            Record record = recordById.get();
            record.setName(recordDto.getName());
            record.setSectors(recordDto.getSectors());
            record.setTermsAgreed(recordDto.isTermsAgreed());
            return RecordDtoMapper.map(recordRepository.save(record));
        } else {
            logger.error("Record: {} is not found in db!", recordDto.getSessionId());
            return null;
        }
    }

    public RecordDto getRecord(long sessionId) {
        Optional<Record> recordById = recordRepository.findById(sessionId);
        if (recordById.isPresent())
            return RecordDtoMapper.map(recordById.get());
        else {
            logger.error("Record: {} is not found in db!", sessionId);
            return null;
        }
    }
}
