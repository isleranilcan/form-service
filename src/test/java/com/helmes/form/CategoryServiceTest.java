package com.helmes.form;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.helmes.form.dto.CategoryDto;
import com.helmes.form.entity.Category;
import com.helmes.form.repository.CategoryRepository;
import com.helmes.form.service.CategoryService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    public static final String CATEGORY_MAPPED_DATA = "src/test/resources/CategoryMappedData.json";
    public static final String CATEGORY_RAW_DATA = "src/test/resources/CategoryRawData.json";

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryService categoryService;

    @Test
    @DisplayName("Should get sectors")
    public void testGetSectors() throws FileNotFoundException {
        //given
        List<CategoryDto> expected = getDataFromJson(CategoryDto.class, CATEGORY_MAPPED_DATA);
        Iterable<Category> rawData = getDataFromJson(Category.class, CATEGORY_RAW_DATA);
        when(categoryRepository.findAll()).thenReturn(rawData);

        //when
        List<CategoryDto> actual = categoryService.getSectors();

        //then
        assertEquals(expected, actual);
    }

    private <T> List<T> getDataFromJson(Class<T> type, String jsonPath) throws FileNotFoundException {
        Gson gson = new Gson();
        BufferedReader br = new BufferedReader(new FileReader(jsonPath));
        Type collectionType = TypeToken.getParameterized(List.class, type).getType();
        return gson.fromJson(br, collectionType);
    }
}
