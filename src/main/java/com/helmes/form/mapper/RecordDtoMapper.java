package com.helmes.form.mapper;

import com.helmes.form.dto.RecordDto;
import com.helmes.form.entity.Record;

public class RecordDtoMapper {

    public static RecordDto map(Record record) {
        return RecordDto.builder()
                .sessionId(record.getId())
                .name(record.getName())
                .sectors(record.getSectors())
                .isTermsAgreed(record.isTermsAgreed())
                .build();
    }
}
