package com.helmes.form.service;

import com.helmes.form.dto.CategoryDto;
import com.helmes.form.mapper.CategoryDtoMapper;
import com.helmes.form.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public List<CategoryDto> getSectors() {
        return CategoryDtoMapper.map(categoryRepository.findAll());
    }
}
